<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("karyawan_model");
		$this->load->model("jabatan_model");
		
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1)
		{
			redirect("auth/index","refresh");
		}
		
		//load validasi
		$this->load->library("form_validation");
	}
	
	public function index()
	{
		$this->listkaryawan();
	}
	
	public function table()
	{
		$data['content']	=	'form/Karyawan/table';
		$this->load->view('home', $data);
	}
	
	public function listkaryawan()
	{
		//proses pencarian data
		//'tombol_cari' name tombol
		if(isset($_POST['tombol_cari']))
		{
			//'cari_data' name textbox
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_karyawan',$data['kata_pencarian']);	
		}
		else
		{
			$data['kata_pencarian'] = $this->session->userdata('session_karyawan');	
		}
		//$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_karyawan'] = $this->karyawan_model->tombolpagination($data['kata_pencarian']);
		$data['content']	=	'form/Karyawan/Home_karyawan';
		$this->load->view('home', $data);
	}
	
	public function input()
	{
		$data['data_nik'] = $this->karyawan_model->nik();
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content']	=	'form/Karyawan/Input_karyawan';
		
		/*if (!empty($_REQUEST)) {
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->save();
			redirect("karyawan/index", "refresh");
		}*/
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->karyawan_model->rules());
		
		if ($validation->run())
		{
			$this->karyawan_model->save();
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Input Data Berhasil !</div>');
			redirect("Karyawan/index", "refresh");	
		}

		$this->load->view('home', $data);
	}
	
	public function detailkaryawan($nik)
	{
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$data['content']	=	'form/Karyawan/Detail_karyawan';
		$this->load->view('home', $data);
	}
	
	public function edit($nik)
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$data['content']	=	'form/Karyawan/Edit_karyawan';
		
		/*if (!empty($_REQUEST)) {
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->update($nik);
			redirect("karyawan/index", "refresh");
		}*/
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->karyawan_model->rules());
		
		if ($validation->run())
		{
			$this->karyawan_model->update($nik);
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Update Data Berhasil !</div>');
			redirect("Karyawan/index", "refresh");	
		}

		$this->load->view('home', $data);
	}
	
	public function delete($nik)
	{
		$m_karyawan = $this->karyawan_model;
		$m_karyawan->delete($nik);
		redirect("karyawan/index", "refresh");
	}
	
	/*public function delete($nik)
	{
		$m_karyawan = $this->karyawan_model;
		//$m_karyawan->delete($nik);
		if ($m_karyawan->delete($nik))
		{
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Delete Data Berhasil !</div>');
			redirect("Karyawan/index", "refresh");	
		}
	}*/
}