<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		//setiap controller kasih sesi login
		//cek sesi login
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1)
		{
			redirect("auth/index","refresh");	
		}
	}
	
	public function index()
	{
		redirect("karyawan/index","refresh");	
	}
}