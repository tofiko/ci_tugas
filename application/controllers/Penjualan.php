<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("karyawan_model");
		$this->load->model("supplier_model");
		$this->load->model("penjualan_model");
		$this->load->model("barang_model");
		
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1)
		{
			redirect("auth/index","refresh");
		}
		
		//load library
		$this->load->library("form_validation");
		$this->load->library('pdf');
	}
	
	public function index()
	{
		$this->listpenjualan();
	}
	
	public function listpenjualan()
	{
		$data['data_penjualan'] = $this->penjualan_model->tampilDataPenjualan();
		$data['content']			=	'form/Penjualan/Home_penjualan';
		$this->load->view('home', $data);
	}
	
	public function input_h()
	{
		//$data['data_notransaksi'] = $this->pembelian_model->notransaksi();
		$data['data_notransaksi'] = $this->penjualan_model->createKodeUrut();
		$data['content']			=	'form/Penjualan/Input_penjualan';
		
		/*if (!empty($_REQUEST)) {
			$m_pembelian_h = $this->pembelian_model;
			$m_pembelian_h->savePembelianHeader();
			
			//panggil ID transaksi terakhir
			$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
			
			redirect("pembelian/input_d/" . $id_terakhir, "refresh");
		}*/
		
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->penjualan_model->rulesH());
		
		if ($validation->run())
		{
			$this->penjualan_model->savePenjualanHeader();
			
			//panggil ID transaksi terakhir
			$id_terakhir = $this->penjualan_model->idTransaksiTerakhir();
			
			redirect("penjualan/input_d/" . $id_terakhir, "refresh");	
		}
		
		$this->load->view('home', $data);
		
		//$this->load->view('input_pembelian', $data);
	}

	/*public function input_d($id)
	{
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id);
		$data['id_header']				= $this->pembelian_model->tampilDataPembelian();
		
		if (!empty($_REQUEST)) {
			$m_pembelian_d = $this->pembelian_model;
			$m_pembelian_d->savePembelianDetail();
			
			//panggil ID transaksi terakhir
			$last_id = $m_pembelian_d->idTransaksiTerakhir();
			
			redirect("pembelian/input_d/" . $last_id, "refresh");
		}
		$this->load->view('input_pembelian_d', $data);
	}*/
	
	public function input_d($id_penjualan_header)
	{
		
		$data['id_header']				= $id_penjualan_header;
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['data_penjualan_detail'] 	= $this->penjualan_model->tampilDataPenjualanDetail($id_penjualan_header);
		$data['content']				= 'form/Penjualan/Input_penjualan_d';
		
		/*if (!empty($_REQUEST)) {
			
			//save detail
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			//exit;
			//proses update stok
			$kode_barang	= $this->input->post('kode_barang');
			$qty			= $this->input->post('qty');
			$this->barang_model->updateStok($kode_barang, $qty);
			
			redirect("pembelian/input_d/" . $id_pembelian_header , "refresh");
		}*/
		
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->penjualan_model->rulesD());
		
		if ($validation->run())
		{
			//save detail
			$this->penjualan_model->savePenjualanDetail($id_penjualan_header);
			
			//proses update stok
			$kode_barang	= $this->input->post('kode_barang');
			$qty			= $this->input->post('qty');
			$this->barang_model->updateStok2($kode_barang, $qty);
			
			redirect("penjualan/input_d/" . $id_penjualan_header , "refresh");
		}
		
		$this->load->view('home', $data);
		//$this->load->view('input_pembelian_d', $data);
	}
	
	public function pencarianreport() 
	{
		$data['content'] = 'form/Report/Pencarian_report_penjualan';
		$this->load->view('home', $data);
	}
	
	public function listreport() 
	{
		if(!empty($_REQUEST)){
			//ambil proses tanggal
			$tgl_awal	= $this->input->post('tgl_awal');
			$tgl_akhir	= $this->input->post('tgl_akhir');
			$data['data_report'] = $this->penjualan_model->tampilreportpenjualan($tgl_awal, $tgl_akhir);
			$data['tgl_awal']	= $tgl_awal;
			$data['tgl_akhir']	= $tgl_akhir;
		
			$data['content'] = 'form/Report/Home_report_penjualan';
			$this->load->view('home', $data);
		}
		else
		{
			redirect("penjualan/pencarianreport", "refresh");
		}
	}
	
	public function cetakpdf($tgl_awal,$tgl_akhir)
	{	
		//ukuran kertas
		$pdf = new FPDF('P','mm','A4');
		//buat halaman baru
		$pdf->AddPage();
		
		$pdf->Cell(10, 5, '', 0, 1);
		//set font yg digunakan
		$pdf->SetFont('Arial','B',20);
		$pdf->Cell(190, 0, 'Toko Jaya Abadi', 0, 0, 'C');
		$pdf->Cell(10, 10, '', 0, 1);
		
		$pdf->SetFont('Arial','B',15); 
		$pdf->Cell(120, 0, 'Data Penjualan', 0, 1, 'C');
		$pdf->Cell(190, 0, $tgl_awal, 0, 0, 'C');
		$pdf->Cell(-150, 0, 'S/D', 0, 0, 'C');
		$pdf->Cell(190, 0, $tgl_akhir, 0, 0, 'C');
		$pdf->Cell(10, 10, '', 0, 1);
		
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(20, 10,'No', 1, 0, 'C');
		$pdf->Cell(35, 10,'No Transksi', 1, 0, 'C');
		$pdf->Cell(35, 10,'Tanggal', 1, 0, 'C');
		$pdf->Cell(30, 10,'Total Barang', 1, 0, 'C');
		$pdf->Cell(30, 10,'Total QTY', 1, 0, 'C');
		$pdf->Cell(40, 10,'Jumlah Nominal', 1, 1, 'C');
		
		$pdf->SetFont('Arial','',12);
		//ambil data dari db
		$no = 0;
		$total = 0;
		$report = $this->penjualan_model->tampilreportpenjualan($tgl_awal, $tgl_akhir);
		foreach($report as $data)
		{
			$no++;
			
			$pdf->Cell(20, 10,$no, 1, 0, 'C');
			$pdf->Cell(35, 10,$data->no_transaksi, 1, 0, 'C');
			$pdf->Cell(35, 10,$data->tanggal, 1, 0, 'C');
			$pdf->Cell(30, 10,$data->total_barang, 1, 0, 'C');
			$pdf->Cell(30, 10,$data->total_qty, 1, 0, 'C');
			$pdf->Cell(40, 10,'Rp. '.number_format($data->total_penjualan), 1, 1, 'C');
			
			$total += $data->total_penjualan;
			
		}
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(150, 10,'Total Keseluruhan', 1, 0, 'C');
		
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(40, 10,'Rp. '.number_format($total), 1, 1, 'C');
		
		//hasil akhir
		$pdf->Output();	
		
	}
	
	/*public function input_d($id_pembelian_header)
	{
		
		$data['id_header']				= $id_pembelian_header;
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
		
		if (!empty($_REQUEST)) {
			
			//save detail
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			//exit;
			//proses update stok
			$kode_barang	= $this->input->post('kode_barang');
			$qty			= $this->input->post('qty');
			$this->barang_model->updateStok($kode_barang, $qty);
			
			redirect("pembelian/input_d/" . $id_pembelian_header , "refresh");
		}
		$this->load->view('input_pembelian_d', $data);
	}*/
}