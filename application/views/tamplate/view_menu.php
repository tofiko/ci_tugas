<header class="header">
    <h1 class="judul" align="center">Toko Jaya Abadi</h1>
    
<div class="menu">
    <ul>
    <li><a href="<?=base_url();?>Tampilhome/listhome">Home</a></li>
    <li class="dropdown"><a href="<?=base_url();?>karyawan/listkaryawan">Master</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
    		<li><a href="<?=base_url();?>jabatan/listjabatan">Data Jabatan</a></li>
    		<li><a href="<?=base_url();?>barang/listbarang">Data Barang</a></li>
    		<li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Data Jenis Barang</a></li>
    		<li><a href="<?=base_url();?>supplier/listsupplier">Data Supplier</a></li>
    	</ul>
    </li>
    <li class="dropdown"><a href="<?=base_url();?>pembelian/listpembelian">Transaksi</a>
    	<ul class="isi-dropdown">
    		<li><a href="<?=base_url();?>pembelian/listpembelian">Pembelian</a></li>
            <li><a href="<?=base_url();?>penjualan/listpenjualan">Penjualan</a></li>
    	</ul>
    </li>
    <li><a href="<?=base_url();?>pembelian/pencarianreport">Report</a></li>
    <li><a href="<?=base_url();?>auth/logout">Log ut</a></li>
    </ul>
</div>
</header>
<br/>