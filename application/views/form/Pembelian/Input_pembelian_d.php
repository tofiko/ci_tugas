<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>pembelian/listpembelian"><i class="fa fa-dashboard"></i> Pembelian</a></li>
        <li class="active">Detail Pembelian</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
			<b>INPUT DATA PEMBELIAN DETAIL</b><br>
		</div>
	</div>
   
<form action="<?=base_url()?>pembelian/input_d/<?= $id_header; ?>" method="post">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Nama Barang</td>
    <td>:</td>
    <td>
      <select name="kode_barang" id="kode_barang">
      <?php foreach($data_barang as $data) 
	  	{
	  ?>
      <option value="<?= $data->kode_barang; ?>"><?= $data->nama_barang; ?></option>
      <?php 
	  	} 
	  ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>Qty</td>
    <td>:</td>
    <td>
      <input type="text" name="qty" id="qty" maxlength="20" autocomplete="off">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  </form>
  <table width="100%" border="0">
      <tr align="center" bgcolor="#CCCCCC">
        <td>No</td>
        <td>Kode Barang</td>
        <td>Nama Barang</td>
        <td>Harga Barang</td>
        <td>Qty</td>
        <td>Jumlah</td>
        </td>
      </tr>
<?php
	$no = 0;
	$total  = 0;
	foreach ($data_pembelian_detail as $data)
	{
	$no++;
?>
      <tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->kode_barang; ?></td>
        <td><?= $data->nama_barang; ?></td>
        <td><?= $data->harga; ?></td>
        <td><?= $data->qty; ?></td>
        <td><?= $data->jumlah; ?></td>
      </tr>
      <?php $total += $data->jumlah;?>
	  <?php
		} 
	  ?>
	  <tr>
	  <td colspan="5" align="right">Total</td>
	  <td align="center">
		<?php
			echo $total;
		?>
      </td>
	</tr>
    
  <tr align="center">
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>pembelian/listpembelian">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
  </tr>
</table>
</div>
