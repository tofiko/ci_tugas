<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>barang/listbarang"><i class="fa fa-dashboard"></i> Barang</a></li>
        <li class="active">Data Barang</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
			<b>DATA BARANG</b><br>
		</div>
    	<form action="<?=base_url();?>barang/listbarang" method="post">
		<ul>
			<h4 align="left">
   				<a href="<?=base_url();?>Barang/input">Input Barang</a><br/>
        	<?php
        		if ($this->session->flashdata('info') == true)
				{
					echo $this->session->flashdata('info');	
				}
			?>
    		</h4>
		</ul>

		<h4 align="right">
			<label for="Cari Nama"></label>
			<input type="text" name="caridata" id="Cari Nama" placeholder="Cari Barang" autocomplete="off">
			<input name="tombol_cari" type="submit" value="cari data">
		</h4>
   
<table width="100%" border="0">
	<tr align="center" bgcolor="#CCCCCC">
    	<td>No</td>
        <td>Kode Barang</td>
        <td>Nama Barang</td>
        <td>Harga Barang</td>
        <td>Nama Jenis</td>
        <td>Aksi</td>
	</tr>
<?php
	$data_posisi = $this->uri->segment(4);		
	$no = $data_posisi;
	if(count($data_barang) > 0)
	{
	foreach ($data_barang as $data)
	{
	$no++;
?>
	<tr align="center">
    	<td><?=$no;?></td>
        <td><?= $data->kode_barang; ?></td>
        <td><?= $data->nama_barang; ?></td>
        <td>Rp. <?= number_format($data->harga_barang); ?> ,-</td>
        <td><?= $data->nama_jenis; ?></td>
        <td><a href="<?=base_url(); ?>barang/detailbarang/<?= $data->kode_barang; ?>">Detail</a>
        	| <a href="<?=base_url(); ?>barang/edit/<?= $data->kode_barang; ?>">Edit</a>
        	| <a href="<?=base_url(); ?>barang/delete/<?= $data->kode_barang; ?>
        		"onclick="return confirm('Yakin Ingin hapus Data?');">Delete
              </a>
        </td>
      </tr>
<?php 
	} 
?>
<tr height="70px">
	<td colspan="7"><b>Halaman :</b> <?=$this->pagination->create_links();?></td>
</tr>
<?php
	}
	else
	{
?>
<tr align="center">
	<td colspan="7">-- Tidak ada data --</td>
</tr>
<?php
	}
?>
</table>
	</form>
	</div>
</div>
