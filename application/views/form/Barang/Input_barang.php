<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>barang/listbarang"><i class="fa fa-dashboard"></i> Barang</a></li>
        <li class="active">Input Barang</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
        	<b>INPUT DATA BARANG</b><br>
        </div>
    </div>
<form action="<?=base_url()?>Barang/input" method="post">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Kode Barang</td>
    <td>:</td>
    <td>
      <?php foreach($data_kodebarang as $data) {
			$n1 = substr($data->kode_barang,-3) +1;
			$hasil = 'BR'.sprintf('%03d' , $n1);
			
		?>
			<input type="text" name="kode_barang" id="kode_barang" value="<?=$hasil;?>" readonly="readonly">      		
      	<?php 
			} 
		?>    
    </td>
  </tr>
  <tr>
  	<td>Nama Barang</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_barang" id="nama_barang" value="<?=set_value('nama_barang');?>" autocomplete="off">
    </td>
  </tr>
  <tr>
    <td>Harga Barang</td>
    <td>:</td>
    <td>
      <input type="text" name="harga_barang" id="harga_barang" value="<?=set_value('harga_barang');?>" autocomplete="off">
    </td>
  </tr>
  <tr>
    <td>Nama Jenis</td>
    <td>:</td>
    <td>
      <select name="kode_jenis" id="kode_jenis">
      <?php foreach($data_jenis_barang as $data) {?>
      	<option value="<?= $data->kode_jenis; ?>"><?= $data->nama_jenis; ?></option>
      <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Barang/listbarang">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
  </tr>
</table>
</form>
</div>
