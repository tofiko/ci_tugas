<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>penjualan/listpenjualan"><i class="fa fa-dashboard"></i> Penjualan</a></li>
        <li class="active">Data Penjualan</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
			<b>DATA PENJUALAN HEADER</b><br>
		</div>
                
	<ul>
    	<h4 align="left">
    	<a href="<?=base_url();?>penjualan/input_h">Input Data Penjualan</a></h4>
    </ul>
    
    <table width="100%" border="0">
		<tr align="center" bgcolor="#CCCCCC">
        	<td>No</td>
        	<td>No Transaksi</td>
        	<td>Tanggal</td>
            <td>Nama Pembeli</td>
        	<td>Aksi</td>
		</tr>
<?php
	$no = 0;
	$total  = 0;
	foreach ($data_penjualan as $data)
	{
	$no++;
?>
      <tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->no_transaksi; ?></td>
        <td><?= $data->tanggal; ?></td>
        <td><?= $data->pembeli; ?></td>
        <td>
        <a href="<?=base_url(); ?>penjualan/input_d/<?= $data->id_jual_h; ?>">Detail</a>
        </td>
      </tr>
<?php
	} 
?>
	</table>
	</div>
</div>