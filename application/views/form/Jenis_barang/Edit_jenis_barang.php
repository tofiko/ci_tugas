<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>jenis_barang/listjenisbarang"><i class="fa fa-dashboard"></i> Jenis Barang</a></li>
        <li class="active">Edit Jenis Barang</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
        	<b>EDIT DATA JENIS BARANG</b><br>
		</div>
	</div>
<?php
	foreach ($detail_jenis_barang as $data) {
		$kode_jenis		= $data->kode_jenis;
		$nama_jenis		= $data->nama_jenis;
	}
?>

<form action="<?=base_url()?>jenis_barang/edit/<?=$kode_jenis;?>" method="post">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Kode Jenis</td>
    <td>:</td>
    <td>
      <input type="text" name="kode_jenis" id="kode_jenis" value="<?=$kode_jenis;?>" maxlength="20" readonly>
    </td>
  </tr>
  <tr>
  	<td>Nama Jenis</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_jenis" id="nama_jenis" value="<?=$nama_jenis;?>" maxlength="100">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>jenis_barang/listjenisbarang">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
  </tr>
</table>
</form>
</div>
