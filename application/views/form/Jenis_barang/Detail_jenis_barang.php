<?php
	foreach ($detail_jenis_barang as $data) {
		$kode_jenis		= $data->kode_jenis;
		$nama_jenis		= $data->nama_jenis;
	}
?>

<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>jenis_barang/listjenisbarang"><i class="fa fa-dashboard"></i> Jenis Barang</a></li>
        <li class="active">Detail Jenis Barang</li>
      </ol>
</section>
<div class="blog">
        	<div class="conteudo">
            	<div class="post-info">
        			<b>DETAIL DATA JENIS BARANG</b><br>
                </div>
            </div>

<table width="80%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff" style="font-weight:bold">
<tr>
	<td>Kode Jenis</td>
    <td>:</td>
    <td><?= $kode_jenis; ?></td>
</tr>
<tr>
	<td>Nama Jenis</td>
    <td>:</td>
    <td><?= $nama_jenis; ?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>jenis_barang/listjenisbarang">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
</tr>
</table>
</div>
