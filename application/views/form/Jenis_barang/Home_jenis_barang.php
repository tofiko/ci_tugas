<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>jenis_barang/listjenisbarang"><i class="fa fa-dashboard"></i> Jenis Barang</a></li>
        <li class="active">Data Jenis Barang</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
        	<b>DATA JENIS BARANG</b><br>
		</div>
<form action="<?=base_url();?>jenis_barang/listjenisbarang" method="post">    
<ul>
    <h4 align="left">
    	<a href="<?=base_url();?>jenis_barang/input">Input Jenis Barang</a>
        <?php
        		if ($this->session->flashdata('info') == true)
				{
					echo $this->session->flashdata('info');	
				}
		?>
    </h4>
</ul>

<h4 align="right">
  	<label for="Cari Nama"></label>
    <input type="text" name="caridata" id="Cari Nama" placeholder="Cari Jenis Barang" autocomplete="off">
    <input name="tombol_cari" type="submit" value="cari data">
</h4>
    
<table width="100%" border="0">
	<tr align="center" bgcolor="#CCCCCC">
        <td>No</td>
        <td>Kode Jenis</td>
        <td>Nama Jenis</td>
        <td>Aksi</td>
	</tr>
<?php
	$data_posisi = $this->uri->segment(4);		
	$no = $data_posisi;
	if(count($data_jenis_barang) > 0)
	{
	foreach ($data_jenis_barang as $data)
	{
	$no++;
?>
	<tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->kode_jenis; ?></td>
        <td><?= $data->nama_jenis; ?></td>
        <td>
        	<a href="<?=base_url(); ?>jenis_barang/detailjenisbarang/<?= $data->kode_jenis; ?>">Detail</a>
        	| <a href="<?=base_url(); ?>jenis_barang/edit/<?= $data->kode_jenis; ?>">Edit</a>
        	| <a href="<?=base_url(); ?>jenis_barang/delete/<?= $data->kode_jenis; ?>
        		"onclick="return confirm('Yakin Ingin hapus Data?');">Delete
         	  </a>
        </td>
	</tr>
<?php 
	} 
?>
<tr height="70px">
	<td colspan="7"><b>Halaman :</b> <?=$this->pagination->create_links();?></td>
</tr>
<?php
	}
	else
	{
?>
<tr align="center">
	<td colspan="7">-- Tidak ada data --</td>
</tr>
<?php
	}
?>
</table>
</form>
	</div>
</div>
