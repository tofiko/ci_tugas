<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-dashboard"></i> Karyawan</a></li>
        <li class="active">Edit Karayawan</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
			<b>EDIT DATA KARYAWAN</b><br>
        </div>
     </div>
<?php
	foreach ($detail_karyawan as $data) {
		$nik			= $data->nik;
		$nama_lengkap	= $data->nama_lengkap;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir		= $data->tgl_lahir;
		$jenis_kelamin	= $data->jenis_kelamin;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
		$kode_jabatan	= $data->kode_jabatan;
		$photo			= $data->photo;
	}
	//Pisah bulan, tanggal, tahun
	$tahun_pisah = substr($tgl_lahir, 0, 4);
	$bulan_pisah = substr($tgl_lahir, 5, 2);
	$tanggal_pisah = substr($tgl_lahir, 8, 2);
?>
<form action="<?=base_url()?>karyawan/edit/<?=$nik;?>" method="post" enctype="multipart/form-data">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td>
      <input type="text" name="nik" id="nik" value="<?=$nik;?>" readonly>
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_lengkap" id="nama_lengkap" value="<?=$nama_lengkap;?>" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input type="text" name="tempat_lahir" id="tempat_lahir" value="<?=$tempat_lahir?>" /></td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      <?php
    	if($jenis_kelamin == 'P'){
			$slc_p = 'SELECTED';
			$slc_l = '';
		}elseif($jenis_kelamin == 'L'){
			$slc_l = 'SELECTED';
			$slc_p = '';
		}else{
			$slc_p = '';
			$slc_l = '';
		}
	?>
      <select name="jenis_kelamin" id="jenis_kelamin">
      	<option <?=$slc_p;?> value="P">Perempuan</option>
        <option <?=$slc_l;?> value="L">Laki-Laki</option>
       </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
   	 <select name="tgl" id="tgl">
     <?php
     	for($tgl_pisah=1;$tgl_pisah<=31;$tgl_pisah++){
			$select_tgl = ($tgl_pisah == $tanggal_pisah) ? 'selected' : '';
	 ?>
     	<option value="<?=$tgl_pisah;?>" <?=$select_tgl;?>><?=$tgl_pisah;?></option>
     <?php
		}
	 ?>
     </select>
      <select name="bln" id="bln">
      <?php
       $bulan_n = array('','Januari','Februari','Maret','April',
	   					'Mei','Juni','Juli','Agustus','September',
						'Oktober','November','Desember');
		for($bln=1;$bln<=12;$bln++){
			$select_bln = ($bln == $bulan_pisah) ? 'selected' : '';
	  ?>
      <option value="<?=$bln;?>" <?=$select_bln;?>><?=$bulan_n[$bln];?> </option>
      <?php
		}
	  ?>
      </select>
      <select name="thn" id="thn">
      <?php
      	for($thn = 1990; $thn <= date('Y');$thn++){
			$select_thn = ($thn == $tahun_pisah) ? 'selected' : '';
	  ?>
      	<option value="<?=$thn;?>" <?=$select_thn;?>><?=$thn;?></option>
      <?php
		}
	  ?>
      </select>

    </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value="<?=$telp?>" /></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"><?=$alamat;?></textarea></td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td>
      <select name="kode_jabatan" id="kode_jabatan">
      	<?php foreach($data_jabatan as $data) {
		  $select_kodejb = ($data->kode_jabatan == $kode_jabatan) ? 'selected' : '';
		?>
      	<option value="<?= $data->kode_jabatan; ?>" <?=$select_kodejb;?>>
			<?= $data->nama_jabatan; ?>
        </option>
      	<?php 
	  		} 
		?>
      </select>
    </td>
  </tr>
  <tr>
    <td>Upload Photo</td>
    <td>:</td>
    <td>
      <input type="file" name="image" id="image">
      <input type="hidden" name="foto_old" id="foto_old" value="<?=$photo;?>">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Karyawan/listkaryawan">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
  </tr>
</table>
</form>
</div>