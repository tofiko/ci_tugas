<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-dashboard"></i> Karyawan</a></li>
        <li class="active">Input Karyawan</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
    	<div class="post-info">
        	<b>INPUT DATA KARYAWAN</b><br>
        </div>
     </div>  
<form action="<?=base_url()?>Karyawan/input" method="post" enctype="multipart/form-data">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td>
		<?php foreach($data_nik as $data) {
			$n1 = substr($data->nik,-3) +1;
			$hasil = date('y').date('m').sprintf('%03d' , $n1);
			//$hasil = $data->nik + 1;
			
		?>
			<input type="text" name="nik" id="nik" value="<?=$hasil;?>" readonly="readonly">      		
      	<?php 
			} 
		?>    	
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_lengkap" id="nama_lengkap" value="<?=set_value('nama_lengkap');?>" autocomplete="off">
    </td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input type="text" name="tempat_lahir" id="tempat_lahir" value="<?=set_value('tempat_lahir');?>" autocomplete="off"></td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      <select name="jenis_kelamin" id="jenis_kelamin">
        <option value="L">Laki-Laki</option>
        <option value="P">Perempuan</option>
      </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
    	<input type="text" id="tgl_lahir" name="tgl_lahir" autocomplete="off">	
    </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value="<?=set_value('telp');?>" autocomplete="off"></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" value="<?=set_value('alamat');?>"></textarea></td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td>
      <select name="kode_jabatan" id="kode_jabatan">
      	<?php foreach($data_jabatan as $data) {?>
      		<option value="<?= $data->kode_jabatan; ?>">
				<?= $data->nama_jabatan; ?>
        	</option>
      	<?php 
			} 
		?>
      </select>
    </td>
  </tr>
  <tr>
    <td>Upload Photo</td>
    <td>:</td>
    <td>
      <input type="file" name="image" id="image">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Karyawan/listkaryawan">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
  </tr>
</table>
</form>
</div>
