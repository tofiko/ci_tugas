<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-dashboard"></i> Karyawan</a></li>
        <li class="active">Data Karayawan</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
    	<div class="post-info">
        	<b>DATA KARYAWAN</b><br>
        </div>
    <form action="<?=base_url();?>karyawan/listkaryawan" method="post">
    <ul>
    <h4 align="left">
    	<a href="<?=base_url();?>karyawan/input">Input Karyawan</a>
    </h4>
    <?php
        	if ($this->session->flashdata('info') == true)
			{
				echo $this->session->flashdata('info');	
			}
	?>
    </ul>
	
    <h4 align="right">
  	<label for="Cari Nama"></label>
    <input type="text" name="caridata" id="Cari Nama" placeholder="Cari Karyawan" autocomplete="off">
    <input name="tombol_cari" type="submit" value="cari data">
  	</h4>
            
	<table width="100%" border="0">
    	<tr align="center" bgcolor="#CCCCCC">
        	<td>No</td>
        	<td>NIK</td>
        	<td>Nama</td>
        	<td>Tempat Lahir</td>
        	<td>Telp</td>
            <td>Foto</td>
        	<td>Aksi</td>
      	</tr>
	<?php
		$data_posisi = $this->uri->segment(4);		
		$no = $data_posisi;
		if(count($data_karyawan) > 0)
		{
		foreach ($data_karyawan as $data)
		{
		$no++;
	?>
    	<tr align="center">
        	<td><?=$no;?></td>
        	<td><?= $data->nik; ?></td>
        	<td><?= $data->nama_lengkap; ?></td>
        	<td><?= $data->tempat_lahir; ?></td>
        	<td><?= $data->telp; ?></td>
            <td>
            	<?php
                	$photo =  $data->photo;
					if(!empty($photo))
					{
				?>
                	<img src="<?=base_url(); ?>resources/fotokaryawan/<?= $data->photo;?>" width="100px" height="100px" />
                <?php
					}
					else
					{
				?>
                	<img src="<?=base_url(); ?>resources/fotokaryawan/default.png" width="100px" height="100px" />
                <?php
					}
				?>
            	
            </td>
        	<td>
        		<a href="<?=base_url(); ?>karyawan/detailkaryawan/<?= $data->nik; ?>">Detail</a>
        		| <a href="<?=base_url(); ?>karyawan/edit/<?= $data->nik; ?>">Edit</a>
        		| <a href="<?=base_url(); ?>karyawan/delete/<?= $data->nik; ?>
        		"onclick="return confirm('Yakin Ingin hapus Data?');">Delete</a>
        	</td>
		</tr>
	<?php 
		} 
	?>
    <tr height="70px">
    	<td colspan="7"><b>Halaman :</b> <?=$this->pagination->create_links();?></td>
    </tr>
    <?php
		}
		else
		{
	?>
    	<tr align="center">
        	<td colspan="7">-- Tidak ada data --</td>
        </tr>
    <?php
		}
	?>
	</table>
    </form>
	</div>
</div>