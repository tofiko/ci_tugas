<?php
	foreach ($detail_karyawan as $data) {
		$nik			= $data->nik;
		$nama_lengkap	= $data->nama_lengkap;
		$tempat_lahir	= $data->tempat_lahir;
		$tgl_lahir		= $data->tgl_lahir;
		$jenis_kelamin	= $data->jenis_kelamin;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
		$kode_jabatan	= $data->nama_jabatan;
	}
?>

<div class="box box-primary">
<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-dashboard"></i> Karyawan</a></li>
        <li class="active">Detail Karayawan</li>
      </ol>
</section>
    <div class="box-body box-profile">
		<?php
            $photo =  $data->photo;
			if(!empty($photo))
			{
		?>
        <img class="profile-user-img img-responsive img-circle" 
		src="<?=base_url(); ?>resources/fotokaryawan/<?= $data->photo;?>" width="100px" height="100px" />
        <?php
			}
		else
		{
		?>
        <img class="profile-user-img img-responsive img-circle" 
		src="<?=base_url(); ?>resources/fotokaryawan/default.png" width="100px" height="100px" />
        <?php
			}
		?>

        <h3 class="profile-username text-center"><b><?= $nama_lengkap; ?></b></h3>

              <p class="text-muted text-center"><?= $kode_jabatan; ?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>NIK</b> <a class="pull-right"><?= $nik; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Tempat Lahir</b> <a class="pull-right"><?= $tempat_lahir; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Tanggal Lahir</b> <a class="pull-right"><?= $tgl_lahir; ?></a>
                </li>
				<li class="list-group-item">
                  <b>Jenis Kelamin</b> 
				  <a class="pull-right">
				  <?php
					if($jenis_kelamin == 'L')
					{
						$value = 'Laki-Laki';
					}
					else
					{
						$value = 'Perempuan';
					}
				  ?>
					<?= $value; ?>
				  </a>
                </li>
				<li class="list-group-item">
                  <b>Telp</b> <a class="pull-right"><?= $telp; ?></a>
                </li>
				<li class="list-group-item">
                  <b>Alamat</b> <a class="pull-right"><?= $alamat; ?></a>
                </li>
              </ul>

              <a href="<?=base_url();?>Karyawan/listkaryawan" class="btn btn-primary btn-block"><b>Kembali</b></a>
            </div>
            <!-- /.box-body -->
          </div>