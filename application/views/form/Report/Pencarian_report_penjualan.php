<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>penjualan/pencarianreport"><i class="fa fa-dashboard"></i> Report</a></li>
        <li class="active">Pencarian Report</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
    	<div class="post-info">
        	<b>Pencarian Report</b><br>
        </div>
     </div>  
<form action="<?=base_url();?>penjualan/listreport" method="post" id="form">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
 	<tr align="center">
    	<td>
        	<p>Tanggal Awal: <input type="text" id="tgl_awal" name="tgl_awal" autocomplete="off"></p>	
        </td>
	</tr>
    <tr align="center">
    	<td>
        	<p>Tanggal Akhir: <input type="text" id="tgl_akhir" name="tgl_akhir" autocomplete="off"></p>	
        </td>
	</tr>
    <tr>
    	<td align="center">
        	<input name="tombol_cari" type="submit" value="Cari Data" id="proses">
        </td>
    </tr>
</table>
</form>
</div>

<script>
	$(document).ready(function()
	{  
		$('#proses').on('click', function(event)
		{
			event.preventDefault();
			var tgl_awal = $('#tgl_awal').val();
			var tgl_akhir = $('#tgl_akhir').val();
			
			if (tgl_awal == '' || tgl_akhir == '')
			{
				alert('Tanggal Tidak Boleh Kosong');	
			}
			else if(new Date(tgl_awal) > new Date(tgl_akhir))
			{
				alert('Format Waktu Salah Input');
			}
			else
			{
				$('#form').submit();	
			}
		});
    });
</script>