<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>penjualan/pencarianreport"><i class="fa fa-dashboard"></i> Report</a></li>
        <li class="active">Data Report</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
			<b>DATA REPORT PENJUALAN</b><br><br>
            <b>Tanggal <?=$tgl_awal;?> S/D <?=$tgl_akhir;?> </b><br><br>
		</div>
    	<form action="<?=base_url();?>penjualan/listreport" method="post">
		<ul>
            <h4 align="left">
   				<a href="<?=base_url();?>penjualan/cetakpdf/<?=$tgl_awal;?>/<?=$tgl_akhir;?>">Cetak PDF</a><br/>
        	<!--?php
        		if ($this->session->flashdata('info') == true)
				{
					echo $this->session->flashdata('info');	
				}
			?-->
    		</h4>
		</ul>
   
<table width="100%" border="0">
	<tr align="center" bgcolor="#CCCCCC">
    	<td>No</td>
        <td>Id Penjualan</td>
        <td>No Transaksi</td>
        <td>Tanggal</td>
        <td>Total Barang</td>
        <td>Total Qty</td>
        <td>Jumlah Nominal Penjualan</td>
	</tr>
<?php
	$data_posisi = $this->uri->segment(4);		
	$no = $data_posisi;	
	$total  = 0;
	if(count($data_report) > 0)
	{
	foreach ($data_report as $data)
	{
	$no++;
?>
	<tr align="center">
    	<td><?=$no;?></td>
        <td><?= $data->id_jual_h; ?></td>
        <td><?= $data->no_transaksi; ?></td>
        <td><?= $data->tanggal;?></td>
        <td><?= $data->total_barang; ?></td>
        <td><?= $data->total_qty; ?></td>
        <td align="right">Rp. <?= number_format($data->total_penjualan); ?> ,-</td>
    </tr>
<?php 
	$total += $data->total_penjualan;
	} 
?>
<table width="100%" border="1" bordercolor="#000000">
	<tr align="center">
		<td colspan="6" align="center"><b>TOTAL KESELURUHAN PENJUALAN</b></td>
		<td align="right">Rp. <b><?= number_format($total); ?></b></td>
	</tr>
</table><br>
	<tr height="70px">
		<td colspan="7"><b>Halaman :</b> <?=$this->pagination->create_links();?></td>
	</tr><br><br>
    <tr align="center">
    	<td>
    		<a href="<?=base_url();?>penjualan/pencarianreport">
    		<input type="button" name="Submit" id="Submit" value="Kembali"></a>
    	</td>
    </tr>  
<?php
	}
	else
	{
?>
<tr align="center">
	<td colspan="7">-- Tidak ada data --</td>
</tr>
<?php
	}
?>
</table>
	</form>
	</div>
</div>
