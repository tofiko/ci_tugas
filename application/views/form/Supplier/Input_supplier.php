<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>supplier/listsupplier"><i class="fa fa-dashboard"></i> Supplier</a></li>
        <li class="active">Input Supplier</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
			<b>INPUT DATA SUPPLIER</b><br>
        </div>
	</div>
     
<form action="<?=base_url()?>supplier/input" method="post">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Kode Supplier</td>
    <td>:</td>
    <td>
      <?php foreach($data_kodesupplier as $data) {
			$n1 = substr($data->kode_supplier,-3) +1;
			$hasil = 'SP'.sprintf('%03d' , $n1);
			
		?>
			<input type="text" name="kode_jabatan" id="kode_jabatan" value="<?=$hasil;?>" readonly="readonly">      		
      	<?php 
			} 
		?>    	
    </td>
  </tr>
  <tr>
    <td>Nama Supplier</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_supplier" id="nama_supplier" maxlength="50" value="<?=set_value('nama_supplier');?>" autocomplete="off">
    </td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" value="<?=set_value('alamat');?>"></textarea></td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value="<?=set_value('keterangan');?>" autocomplete="off"></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>supplier/listsupplier">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
  </tr>
</table>
</form>
</div>
