<?php
	foreach ($detail_supplier as $data) {
		$kode_supplier	= $data->kode_supplier;
		$nama_supplier	= $data->nama_supplier;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
	}
?>

<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>supplier/listsupplier"><i class="fa fa-dashboard"></i> Supplier</a></li>
        <li class="active">Detail Supplier</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
			<b>DETAIL DATA SUPPLIER</b><br>
		</div>
	</div>

<table width="80%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff" style="font-weight:bold">
<tr>
	<td>Kode Supplier</td>
    <td>:</td>
    <td><?= $kode_supplier; ?></td>
</tr>
<tr>
	<td>Nama Supplier</td>
    <td>:</td>
    <td><?= $nama_supplier; ?></td>
</tr>
<tr>
	<td>Alamat</td>
    <td>:</td>
    <td><?= $alamat; ?></td>
</tr>
<tr>
	<td>Telp</td>
    <td>:</td>
    <td><?= $telp; ?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>supplier/listsupplier">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
</tr>
</table>
</div>
