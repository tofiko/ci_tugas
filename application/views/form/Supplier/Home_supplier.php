<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>supplier/listsupplier"><i class="fa fa-dashboard"></i> Supplier</a></li>
        <li class="active">Data Supplier</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
        	<b>DATA SUPPLIER</b><br>
        </div>
<form action="<?=base_url();?>supplier/listsupplier" method="post">        
<ul>
	<h4 align="left">
    	<a href="<?=base_url();?>supplier/input">Input Supplier</a>
        <?php
        	if ($this->session->flashdata('info') == true)
			{
				echo $this->session->flashdata('info');	
			}
		?>
    </h4>
</ul>

<h4 align="right">
  	<label for="Cari Nama"></label>
    <input type="text" name="caridata" id="Cari Nama" placeholder="Cari Supplier" autocomplete="off">
    <input name="tombol_cari" type="submit" value="cari data">
</h4>
    
<table width="100%" border="0">
	<tr align="center" bgcolor="#CCCCCC">
        <td>No</td>
        <td>Kode Supplier</td>
        <td>Nama Supplier</td>
        <td>Alamat</td>
        <td>Aksi</td>
	</tr>
<?php
	$data_posisi = $this->uri->segment(4);		
	$no = $data_posisi;
	if(count($data_supplier) > 0)
	{
	foreach ($data_supplier as $data)
	{
	$no++;
?>
	<tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->kode_supplier; ?></td>
        <td><?= $data->nama_supplier; ?></td>
        <td><?= $data->alamat; ?></td>
        <td>
        	<a href="<?=base_url(); ?>supplier/detailsupplier/<?= $data->kode_supplier; ?>">Detail</a>
        	| <a href="<?=base_url(); ?>supplier/edit/<?= $data->kode_supplier; ?>">Edit</a>
        	| <a href="<?=base_url(); ?>supplier/delete/<?= $data->kode_supplier; ?>
        		"onclick="return confirm('Yakin Ingin hapus Data?');">Delete
              </a>
        </td>
	</tr>
<?php 
	} 
?>
<tr height="70px">
	<td colspan="7"><b>Halaman :</b> <?=$this->pagination->create_links();?></td>
</tr>
<?php
	}
	else
	{
?>
<tr align="center">
	<td colspan="7">-- Tidak ada data --</td>
</tr>
<?php
	}
?>
</table>
</form>
	</div>
</div>
