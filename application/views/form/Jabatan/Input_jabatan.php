<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-dashboard"></i> Jabatan</a></li>
        <li class="active">Input Jabatan</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
    	<div class="post-info">
        	<b>INPUT DATA JABATAN</b><br>
        </div>
    </div>
   
<form action="<?=base_url()?>jabatan/input" method="post">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Kode Jabatan</td>
    <td>:</td>
    <td>
    	<?php foreach($data_kodejabatan as $data) {
			$n1 = substr($data->kode_jabatan,-3) +1;
			$hasil = 'JB'.sprintf('%03d' , $n1);
			
		?>
			<input type="text" name="kode_jabatan" id="kode_jabatan" value="<?=$hasil;?>" readonly="readonly">      		
      	<?php 
			} 
		?>    	
    </td>
  </tr>
  <tr>
    <td>Nama Jabatan</td>
    <td>:</td>
    <td><input type="text" name="nama_jabatan" id="nama_jabatan" value="<?=set_value('nama_jabatan');?>" autocomplete="off"></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>:</td>
    <td><input type="text" name="keterangan" id="keterangan" value="<?=set_value('keterangan');?>" autocomplete="off"></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Jabatan/listjabatan">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>