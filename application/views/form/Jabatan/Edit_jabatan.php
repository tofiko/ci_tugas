<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-dashboard"></i> Jabatan</a></li>
        <li class="active">Edit Jabatan</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
        	<b>EDIT DATA JABATAN</b><br>
        </div>
    </div>
<?php
	foreach ($detail_jabatan as $data) 
	{
		$kode_jabatan	= $data->kode_jabatan;
		$nama_jabatan	= $data->nama_jabatan;
		$keterangan		= $data->keterangan;
	}
?>
<form action="<?=base_url()?>jabatan/edit/<?=$kode_jabatan;?>" method="post">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Kode Jabatan</td>
    <td>:</td>
    <td>
      <input type="text" name="kode_jabatan" id="kode_jabatan" value="<?=$kode_jabatan;?>" readonly>
    </td>
  </tr>
  <tr>
    <td>Nama Jabatan</td>
    <td>:</td>
    <td><input type="text" name="nama_jabatan" id="nama_jabatan" value="<?=$nama_jabatan;?>"/></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>:</td>
    <td><input type="text" name="keterangan" id="keterangan" value="<?=$keterangan;?>" /></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Jabatan/listjabatan">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
  </tr>
  </form>
</table>
</div>
