<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-dashboard"></i> Jabatan</a></li>
        <li class="active">Data Jabatan</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
    	<div class="post-info">
        	<b>DATA JABATAN</b><br>
        </div>
<form action="<?=base_url();?>jabatan/listjabatan" method="post">
<ul>
	<h4 align="left">
    	<a href="<?=base_url();?>jabatan/input">Input Jabatan</a>
        <?php
        	if ($this->session->flashdata('info') == true)
			{
				echo $this->session->flashdata('info');	
			}
		?>
    </h4>
</ul>

<h4 align="right">
	<label for="Cari Nama"></label>
    <input type="text" name="caridata" id="Cari Nama" placeholder="Cari Jabatan" autocomplete="off">
    <input name="tombol_cari" type="submit" value="cari data">
</h4>
    
<table width="100%" border="0">
	<tr align="center" bgcolor="#CCCCCC">
        <td>No</td>
        <td>Kode Jabatan</td>
        <td>Nama Jabatan</td>
        <td>Keterangan</td>
        <td>Aksi</td>
	</tr>
<?php
	$data_posisi = $this->uri->segment(4);		
	$no = $data_posisi;
	if(count($data_jabatan) > 0)
	{
	foreach ($data_jabatan as $data)
	{
	$no++;
?>
	<tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->kode_jabatan; ?></td>
        <td><?= $data->nama_jabatan; ?></td>
        <td><?= $data->keterangan; ?></td>
        <td>
        	<a href="<?=base_url(); ?>jabatan/detailjabatan/<?= $data->kode_jabatan; ?>">Detail</a>
        	| <a href="<?=base_url(); ?>jabatan/edit/<?= $data->kode_jabatan; ?>">Edit</a>
        	| <a href="<?=base_url(); ?>jabatan/delete/<?= $data->kode_jabatan; ?>
        		"onclick="return confirm('Yakin Ingin hapus Data?');">Delete
              </a>
        </td>
	</tr>
<?php 
	} 
?>
<tr height="70px">
	<td colspan="7"><b>Halaman :</b> <?=$this->pagination->create_links();?></td>
</tr>
<?php
	}
	else
	{
?>
<tr align="center">
	<td colspan="7">-- Tidak ada data --</td>
</tr>
<?php
	}
?>
</table>
</form>
	</div>
</div>
    
