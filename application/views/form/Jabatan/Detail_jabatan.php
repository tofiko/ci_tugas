<?php
	foreach ($detail_jabatan as $data) {
		$kode_jabatan	= $data->kode_jabatan;
		$nama_jabatan	= $data->nama_jabatan;
		$keterangan		= $data->keterangan;
	}
?>
<section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-dashboard"></i> Jabatan</a></li>
        <li class="active">Detail Jabatan</li>
      </ol>
</section>
<div class="blog">
	<div class="conteudo">
		<div class="post-info">
			<b>DETAIL DATA JABATAN</b><br>
		</div>
	</div>

<table width="80%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff" style="font-weight:bold">
<tr>
	<td>Kode Jabatan</td>
    <td>:</td>
    <td><?= $kode_jabatan; ?></td>
</tr>
<tr>
	<td>Nama Jabatan</td>
    <td>:</td>
    <td><?= $nama_jabatan; ?></td>
</tr>
<tr>
	<td>Keterangan</td>
    <td>:</td>
    <td><?= $keterangan; ?></td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Jabatan/listjabatan">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
</tr>
</table>
