<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- menu -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url();?>assets/admin-lte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Tofiko Yahsipani</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="<?=base_url();?>karyawan/listkaryawan">
            <i class="fa fa-dashboard"></i> <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-circle-o"></i> Data Karyawan</a></li>
            <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-circle-o"></i> Data Jabatan</a></li>
            <li><a href="<?=base_url();?>barang/listbarang"><i class="fa fa-circle-o"></i> Data Barang</a></li>
            <li><a href="<?=base_url();?>jenis_barang/listjenisbarang"><i class="fa fa-circle-o"></i> Data Jenis Barang</a>
            </li>
            <li><a href="<?=base_url();?>supplier/listsupplier"><i class="fa fa-circle-o"></i> Data Supplier</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="<?=base_url();?>pembelian/listpembelian">
            <i class="fa fa-files-o"></i>
            <span>Transaksi</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>pembelian/listpembelian"><i class="fa fa-circle-o"></i> Pembelian</a></li>
            <li><a href="<?=base_url();?>penjualan/listpenjualan"><i class="fa fa-circle-o"></i> Penjualan</a></li>
          </ul>
        </li>
        
        <li class="treeview active">
          <a href="<?=base_url();?>pembelian/pencarianreport">
            <i class="fa fa-edit"></i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: none;">
            <li><a href="<?=base_url();?>pembelian/pencarianreport"><i class="fa fa-circle-o"></i> Pembelian</a></li>
            <li><a href="<?=base_url();?>penjualan/pencarianreport"><i class="fa fa-circle-o"></i> penjualan</a></li>
          </ul>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>