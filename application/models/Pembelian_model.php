<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_model extends CI_Model
{
	//panggil nama table
	private $_table_header = "pembelian_header";
	private $_table_detail = "pembelian_detail";
	
	public function tampilDataPembelian()
	{
		$query = $this->db->query("SELECT * FROM " . $this->_table_header . " WHERE flag = 1" );
		return $query->result();
	}
	
	public function notransaksi()
	{
		$query = $this->db->query("SELECT * FROM pembelian_header ORDER by no_transaksi DESC LIMIT 1");
		return $query->result();
	}
	
	public function createKodeUrut()
	{
		date_default_timezone_set("Asia/Jakarta");
		$this->db->select('MAX(no_transaksi) as no_transaksi');
		$query	= $this->db->get($this->_table_header);
		$result	= $query->row_array();//hasil bentuk array
		
		$no_transaksi_terakhir = $result[('no_transaksi')];
		
		$label	= 'TR';
		$tahun = substr(date('Y'),-1);
		$bulan = date('m');
		$jam   = date('H');
		if(($jam % 2) == 0)
		{
			$modulus = 'A';	
		}
		else
		{
			$modulus = 'B';	
		}
		$no_urut = substr($no_transaksi_terakhir,-2)+1;
		
		$no_urut_baru = sprintf('%02d' , $no_urut);
		
		$no_transaksi = $label.$tahun.$bulan.$jam.$modulus.$no_urut_baru;
		
		return $no_transaksi;
			
	}
	
	/*public function rulesH()
	{
		return
		[
			[
			//form input
			//field dari name input
			'field' 	=> 'no_transaksi',
			'label'		=> 'Nomor Transaksi',
			'rules' 	=> 'required|max_length[10]',
			'errors'	=>	[
								'required'		=>	'Nomor Transaksi Tidak Boleh Kosong.',
								'max_length'	=>	'Nomor Transaksi Tidak Boleh Lebih Dari 10 Karakter.'
							]
			]
		];	
	}*/
	
	public function savePembelianHeader()
	{
		$data['no_transaksi']	= $this->input->post('no_transaksi');
		$data['kode_supplier']	= $this->input->post('kode_supplier');
		$data['tanggal']		= date('Y-m-d');
		$data['approved']		= 1;
		$data['flag']			= 1;
		$this->db->insert($this->_table_header, $data);
	}
	
	public function idTransaksiTerakhir()
	{
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY
			id_pembelian_h DESC LIMIT 0,1" );
			$data_id = $query->result();
			
			foreach ($data_id as $data){
				$last_id = $data->id_pembelian_h;
			}
		return $last_id;
	}
	
	Public function tampilDataPembelianDetail($id_pembelian_header)
	{			
		$query = $this->db->query(
			"SELECT A. *,  B.nama_barang, B.harga_barang FROM " . $this->_table_detail . " AS A
			INNER JOIN `barang` AS B ON A.kode_barang = B.kode_barang
			WHERE A.`flag` = '1' AND A.`id_pembelian_h`  = ". $id_pembelian_header 
			);
			$data = $query->result();
			
			
		return $query->result();
	}
	
	public function rulesD()
	{
		return
		[
			[
			//form input
			//field dari name input
			'field' 	=> 'qty',
			'label'		=> 'QTY',
			'rules' 	=> 'required|numeric',
			'errors'	=>	[
								'required'		=>	'QTY Tidak Boleh Kosong.',
								'numeric'		=>	'QTY harus berisi angka.'
							]
			]
			/*[
			'field' 	=> 'harga',
			'label'		=> 'Harga Barang',
			'rules' 	=> 'required|numeric',
			'errors'	=>	[
								'required'		=>	'Harga Barang Tidak Boleh Kosong.',
								'numeric'		=>	'Harga Barang harus berisi angka.'
							]
			]*/
		];	
	}
	
	public function savePembelianDetail($id_pembelian_header)
	{
		$qty 					= $this->input->post('qty');
		$kode_barang			= $this->input->post('kode_barang');
		$harga          		= $this->barang_model->cariHarga($kode_barang);
		
		//$data['id_pembelian_d']	= $this->input->post('id_pembelian_d');
		$data['id_pembelian_h']	= $id_pembelian_header;
		$data['kode_barang']	= $kode_barang;
		$data['harga']			= $harga;
		$data['qty']			= $qty;
		$data['jumlah']			= $qty * $harga;
		$data['flag']			= 1;
		
		$this->db->insert($this->_table_detail, $data);
	}
	
	
	public function tampilreportpembelian($tgl_awal, $tgl_akhir)
	{
		
		$this->db->select("A.id_pembelian_h, A.no_transaksi, A.tanggal,
		COUNT(B.kode_barang) AS total_barang, SUM(B.qty) AS total_qty, SUM(B.jumlah) AS total_pembelian");
		$this->db->from("pembelian_header as A");
		$this->db->join("pembelian_detail AS B", "A.id_pembelian_h = B.id_pembelian_h");
		$this->db->where("A.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");	
		$this->db->group_by("A.id_pembelian_h");
		
		$query = $this->db->get();
		return $query->result();
	}
}
