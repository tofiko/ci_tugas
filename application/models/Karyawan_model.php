<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "karyawan";
	
	public function tampilDataKaryawan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	public function nik()
	{
		$query = $this->db->query("SELECT * FROM karyawan ORDER by nik DESC LIMIT 1");
		return $query->result();
	}
	
	public function tampilDataKaryawanPagination($perpage,$uri,$data_pencarian)
	{
		$this->db->select('*');
		if(!empty($data_pencarian))
		{
			$this->db->like('nama_lengkap',$data_pencarian);	
		}
		$this->db->order_by('nik','asc');
		
		$get_data = $this->db->get($this->_table,$perpage,$uri);
		if($get_data->num_rows() > 0)
		{
			return $get_data->result();	
		}
		else
		{
			return null;	
		}
	}
	
	public function tombolpagination($data_pencarian)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->like('nama_lengkap',$data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		//pagination limit
		$pagination['base_url'] = base_url().'karyawan/listkaryawan/load/';
		$pagination['total_rows'] = $hasil;
		$pagination['per_page'] = "3";
		$pagination['uri_segment'] = 4;
		$pagination['num_link'] = 2;
		
		//custom paging configuration
		$pagination['full_tag_open'] = '<div class="pagination">';
		$pagination['full_tag_open'] = '</div>';
		
		$pagination['first_link'] = 'First Page';
		$pagination['first_tag_open'] = '<span class="firstlink">';
		$pagination['first_tag_open'] = '</span>';
		
		$pagination['last_link'] = 'Last Page';
		$pagination['last_tag_open'] = '<span class="lastlink">';
		$pagination['last_tag_open'] = '</span>';
		
		$pagination['next_link'] = 'Next Page';
		$pagination['next_tag_open'] = '<span class="nextlink">';
		$pagination['next_tag_open'] = '</span>';
		
		$pagination['prev_link'] = 'Prev Page';
		$pagination['prev_tag_open'] = '<span class="prevlink">';
		$pagination['prev_tag_open'] = '</span>';
		
		$pagination['cur_tag_open'] = '<span class="curlink">';
		$pagination['cur_tag_open'] = '</span>';
		
		$pagination['num_tag_open'] = '<span class="numlink">';
		$pagination['num_tag_open'] = '</span>';
		
		$this->pagination->initialize($pagination);
		
		$hasil_pagination = $this->tampilDataKaryawanPagination($pagination['per_page'],$this->uri->segment(4),$data_pencarian);
		
		return $hasil_pagination;
	}
	
	public function tampilDataKaryawan2()
	{
		$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataKaryawan3()
	{
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	/*public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}*/
	
	public function detail($nik)
	{
		$query = $this->db->query(
			"SELECT KR. *,  JB.nama_jabatan FROM " . $this->_table . " AS KR
			INNER JOIN `jabatan` AS JB ON KR.kode_jabatan = JB.kode_jabatan
			WHERE KR.`flag` = '1' AND KR.`nik`  = ". $nik 
			);
			$data = $query->result();
			
		return $query->result();
	}
	
	public function rules()
	{
		return
		[
			/*[
			//form input
			//field dari name input
			'field' 	=> 'nik',
			'label'		=> 'NIK',
			'rules' 	=> 'required|max_length[10]',
			'errors'	=>	[
								'required'		=>	'NIK Tidak Boleh Kosong.',
								'max_length'	=>	'NIK Tidak Boleh Lebih Dari 10 Karakter.'
							]
			],*/
				
			[			
			'field' 	=> 'nama_lengkap',
			'label'		=> 'Nama Lengkap',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Nama Lengkap Tidak Boleh Kosong.'
							]
			],
			
			[
			'field' 	=> 'tempat_lahir',
			'label'		=> 'Tempat Lahir',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Tempat Lahir Tidak Boleh Kosong.'
							]
			],
			
			[
			'field' 	=> 'telp',
			'label'		=> 'Telp',
			'rules' 	=> 'required|max_length[15]',
			'errors'	=>	[
								'required'		=>	'Telp Tidak Boleh Kosong.',
								'max_length'	=>	'Telp Tidak Boleh Lebih Dari 15 Karakter.'
							]
			],
				
			[			
			'field' 	=> 'alamat',
			'label'		=> 'Alamat',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Alamat Tidak Boleh Kosong.'
							]
			],
		];	
	}
	
	public function save()
	{
		$nik_karyawan	= $this->input->post('nik');
		
		$data['nik']			= $nik_karyawan;
		$data['nama_lengkap']	= $this->input->post('nama_lengkap');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tgl_lahir']		= $this->input->post('tgl_lahir');
		$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['kode_jabatan']	= $this->input->post('kode_jabatan');
		$data['flag']			= 1;
		$data['photo']			= $this->uploadphoto($nik_karyawan);
		$this->db->insert($this->_table, $data);
		
	}
	
	public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nama_lengkap']	= $this->input->post('nama_lengkap');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tgl_lahir']		= $tgl_gabung;
		$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['kode_jabatan']	= $this->input->post('kode_jabatan');
		$data['flag']			= 1;
		
		if(!empty($_FILES["image"]["name"]))
		{
			$this->hapusphoto($nik);
			$foto_karyawan	= $this->uploadphoto($nik);	
		}
		else
		{
			$foto_karyawan	= $this->input->post('foto_old');	
		}
		
		$data['photo']	= $foto_karyawan;
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	
	private function uploadphoto($nik)
	{
		$date_upload	= date('Ymd');
		$config['upload_path']	= './resources/fotokaryawan/';
		$config['allowed_types']= 'gif|jpg|jpeg|png';
		$config['file_name']	= $date_upload . '-' . $nik;
		$config['overwrite']	= true;
		$config['max_size']		= 1024;//1MB
		//$config['max_width']	= 1024;
		//$config['max_height']	= 768;
		
		$this->load->library('upload',$config);
		
		if($this->upload->do_upload('image'))
		{
			$nama_file	= $this->upload->data("file_name");	
		}
		else
		{
			$nama_file	= "default.png";
		}
		return $nama_file;
	}
	
	private function hapusphoto($nik)
	{
		//cari nama file
		$data_karyawan	= $this->detail($nik);
		foreach ($data_karyawan as $data)
		{
			$nama_file	= $data->photo;
		}
		if($nama_file != "default.jpg")
		{
			$path	= "./resources/fotokaryawan/" .$nama_file;
			
			return @unlink($path);	
		}
	}
	
	public function delete($nik)
	{
		//delete from db
		$this->hapusphoto($nik);
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
	}
	
	/*echo "<pre>";
	print_r($_FILES["image"]); die();
	echo "</pre>";*/
}
